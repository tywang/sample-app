'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /view when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/view");
  });


  describe('view', function() {

    beforeEach(function() {
      browser.get('index.html#!/view');
    });


    it('should render view when user navigates to /view', function() {
      expect(element.all(by.css('[ng-view] div')).first().getText()).
        toMatch(/The item must be return within 30 days/);
    });

    var prev = element(by.css('.orbit-previous')); 
    var next = element(by.css('.orbit-next'));
    var activeSlide = element(by.css('.orbit-bullets button.is-active'));

    it('should render the slide when user navigates click next or prev', function() {
      expect(activeSlide.getAttribute('data-slide')).toBe("0");
      next.click();
      // expect(activeSlide.getAttribute('data-slide')).toBe("1");
      // next.click();
      // expect(activeSlide.getAttribute('data-slide')).toBe("2");
      // prev.click();
      // expect(activeSlide.getAttribute('data-slide')).toBe("1");
      // prev.click();
      // expect(activeSlide.getAttribute('data-slide')).toBe("0");
    }); 

  });

});
