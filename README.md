# `angular-seed` — the seed for AngularJS apps

This project is an sample application built based on [Angular-seed][angular-seed] web app.

## Getting Started

Before you can start to run the application you need to get all the dependencies.
We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

### Run the Application

You can run the application by starting a simple web server locallly:

Step 1 (You need to get generate the JS and CSS first)
```
gulp build
```

Step 2
```
npm start
```

Now browse to the app at [`localhost:8000/index.html`][local-app-url].


## Testing

There are two kinds of tests in the `angular-seed` application: Unit tests and end-to-end tests.

### Running Unit Tests

These unit tests are written in [Jasmine][jasmine],
which we run with the [Karma][karma] test runner. 

You can run the unit test suite by :

```
npm test
```



### Running End-to-End Tests

**Before starting Protractor, open a separate terminal window and run:**

Step 1
```
gulp build
```

Step 2
```
npm start
```

Step 3
```
npm run update-webdriver
```

Step 4
```
npm run protractor
```

This script will execute the end-to-end tests against the application being hosted on the
development server.


