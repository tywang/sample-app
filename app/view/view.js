'use strict';

angular.module('myApp.view', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view', {
    templateUrl: 'view/view.html',
    controller: 'ViewCtrl'
  });
}])

.controller('ViewCtrl', ['$scope','$http', function($scope,$http) {

    $scope.slides = [];
    $scope.init = function() {
      $http.get("/data/item-data.json").then(function(response){
        $scope.item = response.data.CatalogEntryView[0];
  
        $scope.slides.push($scope.item.Images[0].PrimaryImage[0].image);
        angular.forEach($scope.item.Images[0].AlternateImages,function(alt) {
          $scope.slides.push(alt.image);
        });

        $scope.addToCart = function() {
          if($scope.item.purchasingChannelCode == 0 || $scope.item.purchasingChannelCode == 1) {
            return true;
          } else {
            return false;
          }
        }

        $scope.pickUpInStore = function() {
          if($scope.item.purchasingChannelCode == 0 || $scope.item.purchasingChannelCode == 2) {
            return true;
          } else {
            return false;
          }
        }
      });
    }



}]);