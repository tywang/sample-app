'use strict';

describe('myApp.view module', function() {

  beforeEach(module('myApp.view'));

  describe('view controller', function(){

    it('should ....', inject(function($controller, $httpBackend) {
      //spec body
      var $scope = {};
      var view1Ctrl = $controller('ViewCtrl', { $scope: $scope });
      expect(view1Ctrl).toBeDefined();
      expect($scope.slides.length).toBe(0);

      $httpBackend.expectGET("/data/item-data.json").respond(200, {"CatalogEntryView":[{"purchasingChannelCode":0,"Images":[{
            "PrimaryImage":[{"image":"test"}],
            "AlternateImages":[{"image":1},{"image":2}]
        }]}]});
      // Carousel Slide concatenation
      $scope.init();
      $httpBackend.flush();
      expect($scope.slides.length).toBe(3);
      // Test addToCart & pickUpInStore
      $scope.item.purchasingChannelCode = 0;
      expect($scope.addToCart()).toBe(true);
      expect($scope.pickUpInStore()).toBe(true);
      $scope.item.purchasingChannelCode = 1;
      expect($scope.addToCart()).toBe(true);
      expect($scope.pickUpInStore()).toBe(false);
      $scope.item.purchasingChannelCode = 2;
      expect($scope.addToCart()).toBe(false);
      expect($scope.pickUpInStore()).toBe(true);
      $scope.item.purchasingChannelCode = 4;
      expect($scope.addToCart()).toBe(false);
      expect($scope.pickUpInStore()).toBe(false);
    }));



  });
});