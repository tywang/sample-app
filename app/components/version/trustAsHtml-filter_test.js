'use strict';

describe('myApp.version module', function() {
  beforeEach(module('myApp.version'));

  describe('trustAsHtml filter', function() {
    beforeEach(module(function($provide) {
      $provide.value('version', 'TEST_VER');
    }));

    it('should turn string into trust html', inject(function($sce,$filter) {
      var result = $filter('trustAsHtml')('a<span>Hello</span>a');
      expect($sce.getTrustedHtml(result)).toEqual('a<span>Hello</span>a');
    }));

    it('should turn string into Date Object', inject(function(toDateFilter) {
      expect(toDateFilter('11/12/2017')).toEqual(new Date('11/12/2017'));
    }));
  });
});