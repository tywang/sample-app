'use strict';

angular.module('myApp.version.version-directive', [])

.directive('appVersion', ['version', function(version) {
  return function(scope, elm, attrs) {
    elm.text(version);
  };
}])
.directive('rating', [function() {
  return {
    restrict: 'E',
    template: '<span class="rating"></span>',
    link: function (scope, element, attr) {
      attr.$observe('value', function(value){
        var dom = element.find('span');
        if(value) {
          for(var i=1;i<6;i++) {
            if(i > value) {
              dom.append("<b>&#9734;</b>");
            } else {
              dom.append("<b>&#9733;</b>");
            }
          }
        }
      });
    }
  };
}])
.directive('redrawOrbit', ['$timeout',function($timeout){
    return function(scope) {
      if (scope.$last){
        $timeout(function(){Foundation.reInit('orbit');}, 1);
      }
    };
}]);
