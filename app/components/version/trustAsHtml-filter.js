'use strict';

angular.module('myApp.version.interpolate-filter', [])

.filter('trustAsHtml', ['$sce', function($sce) {
  return function(text) {
    return $sce.trustAsHtml(text);
  }
}])
.filter('toDate', [function() {
  return function(text) {
    return new Date(text);
  }
}]);
