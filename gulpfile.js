'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var filter = require('gulp-filter');
var cleanCSS = require('gulp-clean-css');
var mainBowerFiles = require('gulp-main-bower-files');

const dest = 'app/dist';

// JS task
gulp.task('js', function() {
  var files = [
  'bower_components/**/dist/**/modernizr-2.8.3.min.js',
  'bower_components/**/angular.js',
  'bower_components/**/angular-route.js',
  'bower_components/**/dist/jquery.js',
  'bower_components/**/dist/**/foundation.js',
  'app/app.js',
  'app/view/view.js',
  'app/components/**/*.js',
  '!app/components/**/*_test.js',
  ];

  return gulp.src(files)
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dest));
});

gulp.task('css', function(){
  var files = [
    'bower_components/**/html5-boilerplate/dist/css/*.css',
    'bower_components/**/dist/css/foundation.css',
    'app/app.css',
  ];
  
  return gulp.src(files)
      .pipe(concat('main.css'))
      .pipe(cleanCSS())
      .pipe(gulp.dest(dest));
});


// Clean Task
gulp.task('clean', function() {
    return gulp.src([dest+'/*'], {read: false})
        .pipe(clean());
});

gulp.task('build', [ 'clean', 'js', 'css']);